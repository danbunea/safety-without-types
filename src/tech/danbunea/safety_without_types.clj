(ns tech.danbunea.safety-without-types
  (:require
    [clojure.spec.alpha :as s]
    [clojure.spec.test.alpha :as t]
    [clojure.spec.gen.alpha :as gen]
    )
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))






(def matcher (re-matcher #"\d+" "abc12345def"))
;data-spec
(s/def ::id nat-int?)
(s/def ::username (s/and
                    string?
                    #(not (empty? %))
                    #(not (< (count %) 8))
                    ))
(s/def ::password (s/and
                    ::username
                    #(not (< (count %) 8))
                    #(not (nil? (re-find #"\d+" %)));contains at least one digit
                    ))
(s/def ::age (s/and nat-int? (fn [x] (< x 150))))






;function-spec
(s/fdef insert-user :args (s/cat :user (s/keys :req-un [::username ::password ::age])))
(s/fdef delete-user :args (s/cat :id (s/keys :req-un [::id])))
(s/fdef login-user :args (s/cat :user (s/keys :req-un [::username ::password])))
(s/fdef logout-user :args (s/cat :id (s/keys :req-un [::id])))
(s/fdef update-user :args (s/cat :user (s/keys :req-un [::id] :opt-un [::username ::password ::age])))

;functions
(defn insert-user [user])
(defn update-user [user])
(defn delete-user [user])
(defn login-user [user])
(defn logout-user [user])



(comment
  (s/valid? ::age -12);fails
  (s/valid? ::username "");fails
  (s/valid? ::username "1234567");fails
  (s/valid? ::username "12345678")
  (s/valid? ::username nil);fails
  (s/valid? ::username 12345678);fails
  (s/valid? ::password "password");fails
  (s/valid? ::password "password1")

  ;functions
  (t/instrument `insert-user)
  (t/instrument `update-user)
  (t/instrument `delete-user)
  (t/instrument `login-user)
  (t/instrument `logout-user)

  (insert-user {});fails
  (insert-user {:id 1 :username "username" :password "password1"});fails
  (insert-user {:username "username" :password "password1" :age 34})

  (update-user {:id 1 :username "username" :password "password1"});fails
  (update-user {:id 1 :username "us" :password "password1"});fails
  (update-user {:username "username" :password "password1" :age 34});fails
  (update-user {:id 1})

  (login-user {:id 1});fails
  (login-user {:username "user" :password "password1"});fails
  (login-user {:username "username" :password "password1"});fails

  (logout-user {:id 1})
  (logout-user {:id nil});fails

  (delete-user {:id 1})
  (delete-user {:id nil});fails

  (clojure.pprint/pprint (t/check `insert-user))

  (s/def ::user (s/keys :opt-un [::id ::username ::password ::age]))
  (s/exercise ::user 10)
  (gen/generate (s/gen ::user))
  (gen/sample (s/gen ::user))
  )
